hosts_data = {
    "exact_total_matching": True,
    "total_returned": 4,
    "host_list": [{
        "last_reported_time": 1593357488,
        "name": "frontend",
        "is_muted": False,
        "mute_timeout": None,
        "apps": ["agent", "ntp"],
        "tags_by_source": {
            "Datadog": ["env:development:local", "interviewee:john_jrichter.io", "role:web", "host:frontend"]
        },
        "up": True,
        "metrics": {
            "load": None,
            "iowait": None,
            "cpu": None
        },
        "live_metrics": {
            "iowait": 0.03341214917600155,
            "load15": 0.07624999899417162,
            "cpu": 0.3362464904785156
        },
        "sources": ["agent"],
        "host_name": "frontend",
        "id": 2638002863,
        "aliases": ["frontend"]
    }, {
        "last_reported_time": 1593357488,
        "name": "twitter-collector",
        "is_muted": False,
        "mute_timeout": None,
        "apps": ["docker", "trace", "agent", "ntp"],
        "tags_by_source": {
            "Datadog": [
                "env:development:local", "interviewee:john_jrichter.io", "role:data-collection",
                "host:twitter-collector"
            ]
        },
        "up": True,
        "metrics": {
            "load": None,
            "iowait": None,
            "cpu": None
        },
        "live_metrics": {
            "iowait": 0,
            "load15": 0.07714285595076424,
            "cpu": 0.13396453857421875
        },
        "sources": ["agent"],
        "host_name": "twitter-collector",
        "id": 2638020953,
        "aliases": ["twitter-collector"]
    }, {
        "last_reported_time": 1593357485,
        "name": "demo-api",
        "is_muted": False,
        "mute_timeout": None,
        "apps": ["agent", "ntp"],
        "tags_by_source": {
            "Datadog": ["env:development:local", "interviewee:john_jrichter.io", "role:demo:api", "host:demo-api"]
        },
        "up": True,
        "metrics": {
            "load": None,
            "iowait": None,
            "cpu": None
        },
        "live_metrics": {
            "iowait": 0,
            "load15": 0,
            "cpu": 0.1149183000837013
        },
        "sources": ["agent"],
        "host_name": "demo-api",
        "id": 2647349806,
        "aliases": ["demo-api"]
    }, {
        "last_reported_time": 1593357488,
        "name": "postgresql",
        "is_muted": False,
        "mute_timeout": None,
        "apps": ["postgresql", "agent", "ntp"],
        "tags_by_source": {
            "Datadog": ["env:development:local", "interviewee:john_jrichter.io", "role:storage", "host:postgresql"]
        },
        "up": True,
        "metrics": {
            "load": None,
            "iowait": None,
            "cpu": None
        },
        "live_metrics": {
            "iowait": 0.0020899514202028513,
            "load15": 0.06875000009313226,
            "cpu": 0.2216663360595703
        },
        "sources": ["agent"],
        "host_name": "postgresql",
        "id": 2638011282,
        "aliases": ["postgresql"]
    }],
    "total_matching": 4
}
host_live_metrics_data = {
    "host_list": [{
        "live_metrics": {
            "iowait": 0.03341214917600155,
            "load15": 0.07624999899417162,
            "cpu": 0.3362464904785156
        },
        "name": "postgresql"
    }, {
        "live_metrics": {
            "iowait": 0,
            "load15": 0.07714285595076424,
            "cpu": 0.13396453857421875
        },
        "name": "demo-api"
    }, {
        "live_metrics": {
            "iowait": 0,
            "load15": 0.08428571479661125,
            "cpu": 0.1722957066127293
        },
        "name": "chaos-engine"
    }, {
        "live_metrics": {
            "iowait": 0,
            "load15": 0,
            "cpu": 0.1149183000837013
        },
        "name": "frontend"
    }, {
        "live_metrics": {
            "iowait": 0.0020899514202028513,
            "load15": 0.06875000009313226,
            "cpu": 0.2216663360595703
        },
        "name": "twitter-collector"
    }]
}
