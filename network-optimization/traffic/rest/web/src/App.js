import React from 'react';
import axios from 'axios';
import './App.css';

function App() {
  const [hosts, setHosts] = React.useState();
  const [liveMetrics, setLiveMetrics] = React.useState();
  const [totals, setTotals] = React.useState();

  React.useEffect(() => {
    axios.get("http://localhost:8000/api/v1/hosts").then(result => {
      setHosts(result.data);
      console.log(result.data);
    });
  }, []);

  React.useEffect(() => {
    if (hosts !== undefined && hosts.host_list.length > 0) {
      const hostNames = hosts.host_list.map(h => h.name).join(',');
      axios.get(`http://localhost:8000/api/v1/hosts/live_metrics?hosts=${hostNames}`).then(result => {
        setLiveMetrics(result.data);
        console.log(result.data);
      });
    }
  }, [hosts]);

  React.useEffect(() => {
    axios.get("http://localhost:8000/api/v1/hosts/totals").then(result => {
      setTotals(result.data);
      console.log(result.data);
    });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h3>Host Totals</h3>
        {!totals && 'Loading...'}
        {totals && (
          <React.Fragment>
            <div>{`active: ${totals ? totals.total_active : 'UNKNOWN'}`}</div>
            <div>{`up: ${totals ? totals.total_up : 'UNKNOWN'}`}</div>
          </React.Fragment>
        )}
        <h3>Hosts</h3>
        {!hosts && 'Loading...'}
        {hosts && (
          <React.Fragment>
            <div>{`Returned: ${hosts.total_returned}`}</div>
            <div>{`Matching: ${hosts.total_matching}`}</div>
            <ul>
              {hosts.host_list.map(h => <li key={h.name}>{h.name}</li>)}
            </ul>
          </React.Fragment>
        )}
        <h3>Live Metrics</h3>
        {!liveMetrics && 'Loading...'}
        {liveMetrics && (
          liveMetrics.host_list.map(h => (
            <React.Fragment key={h.name}>
              <div>{h.name}</div>
              <ul>
                <li>{`iowait: ${h.live_metrics.iowait}`}</li>
                <li>{`load15: ${h.live_metrics.load15}`}</li>
                <li>{`cpu: ${h.live_metrics.cpu}`}</li>
              </ul>
            </React.Fragment>
          ))
        )}
      </header>
    </div>
  );
}

export default App;
