module.exports = {
    client: {
        tagName: 'gql',
        includes: ['./**/*.js'],
        excludes: ['**/node_modules/*.js'],
        service: {
            name: "my-api",
            url: "http://localhost:8100/graphql"
        }
    }
};