module gitlab.com/johnrichter/datadog-graphql-demo-streaming

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/gorilla/websocket v1.2.0
	github.com/vektah/gqlparser/v2 v2.0.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
