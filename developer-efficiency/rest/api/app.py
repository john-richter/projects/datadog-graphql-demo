from flask import Flask, request
from flask_cors import CORS
from data import hosts, live_metrics

app = Flask(__name__)
CORS(app)


@app.route('/api/v1/hosts')
def hosts_main():
    return hosts


@app.route('/api/v1/hosts/live_metrics')
def hosts_live_metrics():
    host_list = request.args.get('hosts', [])
    if isinstance(host_list, str):
        host_list = host_list.split(',')

    return {'host_list': [h for h in live_metrics['host_list'] if h['name'] in host_list]}


@app.route('/api/v1/hosts/totals')
def hosts_totals():
    return {
        'total_active': len([h for h in hosts['host_list'] if not h['is_muted']]),
        'total_up': len([h for h in hosts['host_list'] if h['up']])
    }


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
