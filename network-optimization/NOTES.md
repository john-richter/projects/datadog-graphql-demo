# Setup

1. Start `traffic/rest/api` and `traffic/graphql/api`

```
cd ~/Development/interviews/datadog/demos/network-optimization
workon dd-demo

python traffic/rest/api/app.py
python traffic/graphql/api/app.py
```

2. Start `traffic/rest/web` and `traffic/graphql/web`

```
cd ~/Development/interviews/datadog/demos/network-optimization/traffic/rest/web
yarn start

cd ~/Development/interviews/datadog/demos/network-optimization/traffic/graphql/web
yarn start
```

3. Start `streaming/rest` and `streaming/graphql/api`

```
cd ~/Development/interviews/datadog/demos/network-optimization/streaming/rest
workon dd-demo
<START DOCKER>
./start.sh

cd ~/Development/interviews/datadog/demos/network-optimization/streaming/graphql/api
go build .
./datadog-graphql-demo-streaming
```

4. Start `graphql/graphql/web`

```
cd ~/Development/interviews/datadog/demos/network-optimization/streaming/graphql/web
yarn start
```

# Notes

> Goal: Show how REST leads to higher request counts and higher data usage on average

## Traffic

**I've created two APIs to show. The first is REST**
- Show `traffic/rest/api/app.py` and `curl localhost:8000/api/v1/hosts | jq '.'` it
- Show `traffic/rest/web/App.js` to show how REST is typically used in web UIs
- Open `localhost:3000` to show the data loading.
    - Open network panel and refresh using `Fast3G`, **emphasizing** request count and data size

**I've created a duplicate API, except in GQL**
- Show `traffic/graphql/api/schema.py`, open http://localhost:8001/graphql, and run query the 
  mirror query from `traffic/graphql/demo_queries/1-...gql`
- Explain how this can be optimzed even more using `traffic/graphql/demo_queries/2-...gql`
    - This one unifies `totals` under `hosts` and moves live metrics to each host
- In fact, we can even target the exact `hosts` using `traffic/graphql/demo_queries/2-...gql`
    - This uses `names` argument in `hosts`
- We can even optimize this further using variables and directives, `@skip` and `@include`
    - `traffic/graphql/demo_queries/4-...gql`

- Show `traffic/graphql/web/App.js` to show how GQL is typically used in web UIs
- Open http://localhost:3001 to show the data loading
    - Open network panel and refresh using `Fast3G`, **emphasizing** request count and data size

Compare REST vs GraphQL queries.
 - Time to load (**~66% faster**)
 - Size of data (**~63% lower**)
 - Number of requests (**66% fewer**)

> Your AWS API Gateway bill just got a whole lot lighter
> Your customers are happy your product just became faster to load and more responsive

But what about a company like Datadog, that has to surface an incredible amount of data to 
customers in real-time?

**How does GraphQL handle streaming?**

## Streaming

**Again, I've created two APIs to show you, Let's start with REST**
- Show `streaming/rest/app.py`, `sse` endpoint, and explain how the live metrics are 
  randomly generated every 10 sec
    - Open `localhost:8002/api/v1/hosts/live/metrics` and watch metrics flow in
- Explain that this is a solution specific to the endpoint
    - You're responsible for creating everything: the data model, protocol, transport (ws, unix
      socket, etc), etc
    - Must be repeated for each new resource your want to be able to stream.

**How does GQL handle this?**
- Show `streaming/graphql/api/graph/schema/schema.graphql` and highlight `subscriptions`
    - The notion of data streams is native to GQL. Subscriptions automatically allow you to stream 
      any data you want, and even do it over the same connection
- Show `streaming/graphql/api/graph/schema.resolvers.go`, `HostLiveMetricsChanged`
- Show `streaming/graphql/api/server.go` to show I only configured the transport as a web socket
- Open http://localhost:8003/ and run query from
  `streaming/graphql/api/demo_queries/subscription.graphql`
- Open http://localhost:3002, show network panel, and let the page refresh really quick. It 
  all comes over the subscr

All you need to do is define a field on the `Subscription` type, implement the resolver, 
and let GraphQL do the rest.
