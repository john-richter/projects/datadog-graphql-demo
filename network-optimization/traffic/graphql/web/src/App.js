import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import './App.css';

const HOSTS_QUERY_1 = gql`{
  hosts {
    totals {
      totalUp
      totalActive
      totalReturned
      totalMatching
    }
    hostList {
      name
      liveMetrics {
        iowait
        load15
        cpu
      }
    }
  }
}`;

function App() {
  const { loading, error, data } = useQuery(HOSTS_QUERY_1);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const totals = data.hosts.totals;
  const hosts = data.hosts.hostList;

  return (
    <div className="App">
      <header className="App-header">
        <h3>Host Totals</h3>
        <div>{`Returned: ${totals.totalReturned}`}</div>
        <div>{`Matching: ${totals.totalMatching}`}</div>
        <div>{`Active: ${totals.totalActive}`}</div>
        <div>{`Up: ${totals.totalUp}`}</div>
        <h3>Hosts</h3>
        <ul>
          {hosts.map(h => <li key={h.name}>{h.name}</li>)}
        </ul>
        <h3>Live Metrics</h3>
        {hosts.map(h => (
          <React.Fragment key={h.name}>
            <div>{h.name}</div>
            <ul>
              <li>{`iowait: ${h.liveMetrics.iowait}`}</li>
              <li>{`load15: ${h.liveMetrics.load15}`}</li>
              <li>{`cpu: ${h.liveMetrics.cpu}`}</li>
            </ul>
          </React.Fragment>
        ))}
      </header>
    </div>
  );
}

export default App;
