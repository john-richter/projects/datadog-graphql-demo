package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"log"
	"time"

	"gitlab.com/johnrichter/datadog-graphql-demo-streaming/graph/generated"
	"gitlab.com/johnrichter/datadog-graphql-demo-streaming/graph/model"
)

func (r *queryResolver) Hosts(ctx context.Context, names []string) ([]*model.Host, error) {
	return filteredHosts(names), nil
}

func (r *queryResolver) HostLiveMetrics(ctx context.Context, names []string) ([]*model.HostLiveMetricsMap, error) {
	hosts := filteredHosts(names)
	metrics := []*model.HostLiveMetricsMap{}
	for _, h := range hosts {
		metrics = append(metrics, &model.HostLiveMetricsMap{Name: h.Name, LiveMetrics: h.LiveMetrics})
	}
	return metrics, nil
}

func (r *subscriptionResolver) HostLiveMetricsChanged(ctx context.Context, names []string) (<-chan []*model.HostLiveMetricsMap, error) {
	stream := make(chan []*model.HostLiveMetricsMap)
	hosts := filteredHosts(names)
	go func() {
		log.Println("starting stream")
		for {
			select {
			case <-ctx.Done():
				return
			default:
				metrics := []*model.HostLiveMetricsMap{}
				for _, h := range hosts {
					metrics = append(metrics, &model.HostLiveMetricsMap{Name: h.Name, LiveMetrics: h.LiveMetrics})
				}
				stream <- metrics
				time.Sleep(10 * time.Second)
			}
		}
	}()
	go func() {
		<-ctx.Done()
		close(stream)
		log.Println("closing stream")
	}()
	return stream, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
