import asyncio
import signal
import random
from flask import Flask, request
from flask_cors import CORS
from flask_sse import sse
from flask_apscheduler import APScheduler
from apscheduler.schedulers.gevent import GeventScheduler
from apscheduler.jobstores.memory import MemoryJobStore
from data import hosts, live_metrics

app = Flask(__name__)
app.register_blueprint(sse, url_prefix='/api/v1/hosts/live/metrics')
# app.register_blueprint(sse, url_prefix='/api/stream') # Or should we use this? Or both?
CORS(app)


@app.route('/api/v1/hosts')
def hosts_main():
    return hosts


@app.route('/api/v1/hosts/live_metrics')
def hosts_live_metrics():
    host_list = request.args.get('hosts', [])
    if isinstance(host_list, str):
        host_list = host_list.split(',')

    return {'host_list': [h for h in live_metrics['host_list'] if h['name'] in host_list]}


@app.route('/api/v1/hosts/totals')
def hosts_totals():
    return {
        'total_active': len([h for h in hosts['host_list'] if not h['is_muted']]),
        'total_up': len([h for h in hosts['host_list'] if h['up']])
    }


def gen_live_metrics():
    with app.app_context():
        new_metrics = []
        for h in live_metrics['host_list']:
            new_metrics.append({
                'name': h['name'],
                'live_metrics': {
                    'cpu': random.random(),
                    'iowait': random.random(),
                    'load15': random.random()
                }
            })
        sse.publish(new_metrics)


class Config(object):
    JOBS = [{'id': 'gen_live_metrics', 'func': 'app:gen_live_metrics', 'trigger': 'interval', 'seconds': 10}]
    SCHEDULER_API_ENABLED = True
    SCHEDULER_JOBSTORES = {'default': MemoryJobStore()}

    SCHEDULER_EXECUTORS = {'default': {'type': 'threadpool', 'max_workers': 20}}

    SCHEDULER_JOB_DEFAULTS = {'coalesce': False, 'max_instances': 3}

    SCHEDULER_API_ENABLED = True
    REDIS_URL = "redis://localhost"


app.config.from_object(Config())

geventScheduler = GeventScheduler()
geventScheduler.configure(jobstores={'default': MemoryJobStore()})
scheduler = APScheduler(geventScheduler, app)
scheduler.start()
