import graphene
import asyncio
from graphene import relay
from data import hosts_data, host_live_metrics_data
from random import random


class HostLiveMetrics(graphene.ObjectType):
    load15 = graphene.Float()
    iowait = graphene.Float()
    cpu = graphene.Float()


class HostLiveMetricsMap(graphene.ObjectType):
    name = graphene.String()
    live_metrics = graphene.Field(HostLiveMetrics)

    def resolve_live_metrics(parent, info, **args):
        return HostLiveMetrics(**parent.live_metrics)


class HostMetrics(graphene.ObjectType):
    load = graphene.Float()
    iowait = graphene.Float()
    cpu = graphene.Float()


class SourceTags(graphene.ObjectType):
    Datadog = graphene.List(graphene.String)


class HostTotals(graphene.ObjectType):
    total_active = graphene.Int()
    total_up = graphene.Int()
    total_returned = graphene.Int()
    total_matching = graphene.Int()


class Host(graphene.ObjectType):
    '''
    A hardware or virtual machine instance
    '''
    last_reported_time = graphene.Int()
    # name = graphene.String(description='The name of the host')
    name = graphene.String(deprecation_reason='This field has been renamed. Use "hostId" instead')
    host_id = graphene.String()
    is_muted = graphene.Boolean()
    mute_timeout = graphene.Int()
    apps = graphene.List(graphene.String)
    tags_by_source = graphene.Field(SourceTags)
    up = graphene.Boolean()
    metrics = graphene.Field(HostMetrics)
    live_metrics = graphene.Field(HostLiveMetrics)
    sources = graphene.List(graphene.String)
    host_name = graphene.String()
    id = graphene.ID()
    aliases = graphene.List(graphene.String)

    def resolve_host_id(parent, info, **args):
        return parent.name

    def resolve_metrics(parent, info, **args):
        return HostMetrics(**parent.metrics)

    def resolve_live_metrics(parent, info, **args):
        return HostLiveMetrics(**parent.live_metrics)

    def resolve_tags_by_source(parent, info, **args):
        return SourceTags(**parent.tags_by_source)


class HostTotalsQuery(graphene.ObjectType):
    total_active = graphene.Int()
    total_up = graphene.Int()


class HostQuery(graphene.ObjectType):
    exact_total_matching = graphene.Boolean()
    total_returned = graphene.Int()
    total_matching = graphene.Int()
    host_list = graphene.List(Host)
    totals = graphene.Field(HostTotals)

    def resolve_host_list(parent, info, **args):
        return [Host(**h) for h in parent.host_list]

    def resolve_totals(parent, info, **args):
        return HostTotals(
            total_returned=parent.total_returned,
            total_matching=parent.total_matching,
            total_active=len([h for h in parent.host_list if not h['is_muted']]),
            total_up=len([h for h in parent.host_list if h['up']])
        )


class HostLiveMetricsQuery(graphene.ObjectType):
    host_list = graphene.List(HostLiveMetricsMap)

    def resolve_host_list(parent, info, **args):
        return [HostLiveMetricsMap(**h) for h in parent.host_list]


class Query(graphene.ObjectType):
    hosts = graphene.Field(HostQuery, names=graphene.List(graphene.String))
    host_live_metrics = graphene.Field(HostLiveMetricsQuery, names=graphene.List(graphene.String))
    host_totals = graphene.Field(HostTotalsQuery)

    def resolve_hosts(root, info, names=None):
        filtered_hosts = dict(hosts_data)
        if names is not None:
            filtered_hosts['host_list'] = [h for h in filtered_hosts['host_list'] if h['name'] in names]
            filtered_hosts['total_matching'] = len(filtered_hosts['host_list'])
            filtered_hosts['total_returned'] = len(filtered_hosts['host_list'])
        return HostQuery(**filtered_hosts)

    def resolve_host_live_metrics(root, info, names=None):
        filtered_live_metrics = dict(host_live_metrics_data)
        if names is not None:
            filtered_live_metrics['host_list'] = [h for h in filtered_live_metrics['host_list'] if h['name'] in names]
        return HostLiveMetricsQuery(**filtered_live_metrics)

    def resolve_host_totals(root, info):
        return HostTotalsQuery(
            total_active=len([h for h in hosts_data['host_list'] if not h['is_muted']]),
            total_up=len([h for h in hosts_data['host_list'] if h['up']])
        )


class Subscription(graphene.ObjectType):
    host_live_metrics_changed = graphene.List(HostLiveMetricsMap, names=graphene.List(graphene.String))

    async def subscribe_host_live_metrics_changed(root, info, names=None):
        host_names = [h['name'] for h in hosts_data['host_list']]
        if names is not None:
            host_names = [h for h in host_names if h in names]
        while True:
            metrics_maps = []
            for name in host_names:
                metric_map = HostLiveMetricsMap(
                    name=name, live_metrics=HostLiveMetrics(cpu=random(), iowait=random(), load15=random())
                )
                metrics_maps.append(metric_map)
            yield metrics_maps
            await asyncio.sleep(5)


schema = graphene.Schema(query=Query, subscription=Subscription)
