from flask import Flask, request
from flask_graphql import GraphQLView
from flask_cors import CORS
from schema import schema

app = Flask(__name__)
app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))
CORS(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8100)
