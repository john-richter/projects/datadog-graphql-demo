# Setup

1. Start basic server
```
cd ~/Development/interviews/datadog/demos/developer-efficiency
workon dd-demo
python graphql/api/app.py
```

2. Start Paddock Tech server
```
cd ~/Development/personal/paddocktech/1lap/graphql-schema
yarn app:start:dev
```

3. Start Voyager
```
cd ~/Development/experiments/graphql-voyager
workon dd-demo
python -m http.server 8101 --directory demo-dist
```

4. Start GQL Editor
```
cd ~/Development/experiments/graphql-editor
yarn start
```

4. Generate all schemas and docs
```
cd ~/Development/interviews/datadog/demos/developer-efficiency/graphql/gen

yarn graphql-codegen -c ./codegen-basic.yml
yarn jsdoc -d ./generated/basic/jsdoc ./generated/basic/schema.jsdoc
python -m http.server 8102 --directory graphql/gen/generated/basic/jsdoc

yarn graphql-codegen -c ./codegen-advanced.yml
yarn jsdoc -d ./generated/advanced/jsdoc ./generated/advanced/schema.jsdoc
python -m http.server 8103 --directory graphql/gen/generated/advanced/jsdoc

yarn graphql-codegen -c ./codegen-github.yml
yarn jsdoc -d ./generated/github/jsdoc ./generated/github/schema.jsdoc
python -m http.server 8104 --directory graphql/gen/generated/github/jsdoc
```

5. Load all webpages and schemas into pages

- Open Voyager for basic and advanced: http://localhost:8101/
    - Copy introspection query to the web UI of voyager and give it this:
        - `{"data": <contents of introspection>}`
- Open GraphQL Editor for basic and advanced: http://localhost:1568/
    - Copy the `schema` file and load it as code
- Open GraphQL Editor for GitHub: https://app.graphqleditor.com/explore
    - Copy the `schema` file and load it as code

6. Load Swagger Pet Store: https://editor.swagger.io/

# Other Documentation Tools

https://github.com/gjtorikian/graphql-docs
- Example: https://www.gjtorikian.com/graphql-docs/

https://github.com/2fd/graphdoc

# Generate Basic docs

```
yarn graphql-codegen -c ./codegen-basic.yml
yarn jsdoc -d ./generated/basic/jsdoc ./generated/basic/schema.jsdoc
python -m http.server 8102 --directory generated/basic/jsdoc
```

# Generate Advanced docs

```
yarn graphql-codegen -c ./codegen-advanced.yml
yarn jsdoc -d ./generated/advanced/jsdoc ./generated/advanced/schema.jsdoc
python -m http.server 8103 --directory generated/advanced/jsdoc
```

# Generate Github docs

Edit `./codegen-github.yml` and add bearer token

```
yarn graphql-codegen -c ./codegen-github.yml
yarn jsdoc -d ./generated/github/jsdoc ./generated/github/schema.jsdoc
python -m http.server 8104 --directory generated/github/jsdoc
```

# Notes

> Goal: Tie back to developer effiency for both customers and datadog engineers

**Typical workflow**
1. Create and implement the API (**Show rest/api/app.py**)
2. Create docs for the product website (human error prone)
3. Maybe create schema for the API (**Show Swagger Pet Store**)
4. Maaaaaybe create Swagger like docs if you have the time (**Show Swagger Pet Store**)
5. Make changes to API, update version, repeat steps 1-5 
   (**mention different methods of versioning (headers, url path, etc)**)

Implementation, documentation, and specification are not bound and quickly drift.

In a Service Oriented Architectures where cross-company API integrations are standard,
this process quickly becomes hard to manage and evolve.

**GraphQL Workflow**
1. Create GQL Schema and document it using built-in functionality 
   (**show graphql/gen/generated/basic/schema.graphql**)
2. Implement server to the GQL spec (**show graphql/api/schema.py**)
3. (optionally) Share the GQL spec publically
4. Make changes to Schema, repeat steps 2-4

The burden of documentation is removed. Burden of schema generation can be reduced to if you choose a code-first vs schema-first based server (**graphene vs gqlgen (go)**). Our API here is code-first!

- Open GiQL for both and show docs panel
- Generate docs from it. **Voyager and GraphQL Editor**

Docs are in front of developers at all times.
- Show how those docs are visible from the IDE (**App.js gql statement**)

Versioning is field-based
- Deprecate `name` field using directives. Show how that reflects in developer environment.
- **Run `yarn graphql-codegen -c ./codegen-basic.yml` in built in terminal after schema changes**
- (optional) Explain how directives can be added for more context

Back in our Service Oriented Architecture, our API integrations can now rely on GraphQL resources
from other services directly. No need to understand keep up with another API.
- More advanced topics if they are interested: `Federation`, `Schema Stiching`

**If time show these real world examples**

## Real world demos

### Paddock Tech

- `graphql/gen/generated/advanced/schema.graphql`
- `~/Development/personal/paddocktech/1lap/graphql-schema`
    - To show implementation
    - How services communicate to each other
- http://localhost:4000/playground to show docs and such

```
query {
  tracks(first: 3) {
    edges {
      node {
        name
        urlSlug
        location {
          city
          region
          country
          latitude
          longitude
          timezone
        }
      }
    }
  }
}
```

### Github's Public GraphiQL

https://developer.github.com/v4/explorer/
