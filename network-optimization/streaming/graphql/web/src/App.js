import React from 'react';
import { useQuery, useSubscription } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import './App.css';

const HOSTS_QUERY = gql`{
  hosts {
    name
    liveMetrics {
      iowait
      load15
      cpu
    }
  }
}`;

const HOST_LIVE_METRICS_SUBSCRIPTION = gql`
  subscription hostMetricsChangedSub($names: [String!]) {
    hostLiveMetricsChanged(names: $names) {
      name
      liveMetrics {
        cpu
        iowait
        load15
      }
    }
  }
`;

function App() {
  // const liveMetrics, setLiveMetrics = React.useState();
  const { loading: hostsLoading, error: hostsError, data: hostsData } = useQuery(HOSTS_QUERY);
  const { data: hostLiveMetricsData, loading: loadingSubscription, error: errorSubscription } = useSubscription(
    HOST_LIVE_METRICS_SUBSCRIPTION,
    {
      variables: { names: hostsData ? hostsData.hosts.map(h => h.name) : undefined },
      fetchPolicy: 'no-cache',
      shouldResubscribe: true
    }
  );

  if (hostsLoading) return <p>Loading...</p>;
  if (hostsError) return <p>Error :(</p>;

  const hosts = hostsData.hosts;
  let hostLiveMetrics = [];
  if (loadingSubscription || errorSubscription) {
    hostLiveMetrics = hosts.map(h => ({ name: h.name, liveMetrics: h.liveMetrics }));
  } else {
    hostLiveMetrics = hostLiveMetricsData.hostLiveMetricsChanged;
  }

  return (
    <div className="App">
      <header className="App-header">
        <h3>Hosts</h3>
        <ul>
          {hosts.map(h => <li key={h.name}>{h.name}</li>)}
        </ul>
        <h3>Live Metrics</h3>
        {hosts.map(h => {
          const hostMetrics = hostLiveMetrics.find(m => m.name === h.name).liveMetrics;
          return (
            <React.Fragment key={h.name}>
              <div>{h.name}</div>
              <ul>
                <li>{`iowait: ${hostMetrics.iowait}`}</li>
                <li>{`load15: ${hostMetrics.load15}`}</li>
                <li>{`cpu: ${hostMetrics.cpu}`}</li>
              </ul>
            </React.Fragment>
          );
        })}
      </header>
    </div>
  );
}

export default App;
