package graph

import (
	"math/rand"
	"time"

	"gitlab.com/johnrichter/datadog-graphql-demo-streaming/graph/model"
)

var hostsData = []*model.Host{
	&model.Host{
		Name:        "frontend",
		Up:          &[]bool{true}[0],
		LiveMetrics: &model.HostLiveMetrics{CPU: 0.1149183000837013, Load15: 0, Iowait: 0},
	},
	&model.Host{
		Name: "postgresql",
		Up:   &[]bool{true}[0],
		LiveMetrics: &model.HostLiveMetrics{
			CPU: 0.3362464904785156, Load15: 0.07624999899417162, Iowait: 0.03341214917600155,
		},
	},
	&model.Host{
		Name: "twitter-collector",
		Up:   &[]bool{true}[0],
		LiveMetrics: &model.HostLiveMetrics{
			CPU: 0.2216663360595703, Load15: 0.06875000009313226, Iowait: 0.0020899514202028513,
		},
	},
}

func init() {
	go generateLiveMetrics()
}

func generateLiveMetrics() {
	for {
		for _, h := range hostsData {
			h.LiveMetrics.CPU = rand.Float64()
			h.LiveMetrics.Load15 = rand.Float64()
			h.LiveMetrics.Iowait = rand.Float64()
		}
		time.Sleep(10 * time.Second)
	}
}

func filteredHosts(names []string) []*model.Host {
	if names == nil {
		return hostsData
	}

	hosts := []*model.Host{}
	for _, h := range hostsData {
		for _, n := range names {
			if h.Name == n {
				hosts = append(hosts, h)
			}
		}
	}
	return hosts
}
